#!/usr/bin/env /lib/init/init-d-script
### BEGIN INIT INFO
# Provides:          prometheus-smokeping-prober
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Prometheus style "smokeping" prober
# Description:       Prometheus exporter which sends a series of ICMP (or UDP)
#                    pings to targets and records the responses in Prometheus
#                    histogram metrics.
### END INIT INFO

# Author: Daniel Swarbrick <dswarbrick@debian.org>

DESC="Prometheus style \"smokeping\" prober"
NAME=prometheus-smokeping-prober
USER=prometheus
GROUP=$USER
DAEMON=/usr/bin/$NAME
PIDFILE=/run/prometheus/$NAME.pid
LOGFILE=/var/log/prometheus/$NAME.log

START_ARGS="--no-close --background --make-pidfile"
STOP_ARGS="--remove-pidfile"

do_start_prepare() {
  mkdir -p $(dirname $PIDFILE)
}

do_start_cmd_override() {
  start-stop-daemon --start --quiet --oknodo \
    --exec $DAEMON --pidfile $PIDFILE --user $USER --group $GROUP \
    --chuid $USER:$GROUP $START_ARGS -- $ARGS >>$LOGFILE 2>&1
}

do_stop_cmd_override() {
  start-stop-daemon --stop --quiet --oknodo --retry=TERM/30/KILL/5 \
    --exec $DAEMON --pidfile $PIDFILE --user $USER $STOP_ARGS
}

alias do_reload=do_reload_sigusr1
