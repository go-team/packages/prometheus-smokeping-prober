# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the prometheus-smokeping-prober package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: prometheus-smokeping-prober\n"
"Report-Msgid-Bugs-To: prometheus-smokeping-prober@packages.debian.org\n"
"POT-Creation-Date: 2021-02-03 15:43+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Enable additional network privileges for ICMP probing?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"/usr/bin/prometheus-smokeping-prober requires the CAP_NET_RAW capability to "
"be able to send out crafted packets to targets for ICMP probing."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"ICMP probing will not work unless this option is enabled, or prometheus-"
"smokeping-prober runs as root."
msgstr ""
